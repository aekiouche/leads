

#include <fcntl.h>
#include <fstream>
#include "graph.h"
#include "io.h"
#include <iostream>
#include "param.h"
#include <string>
#include <sstream>
#include <tuple>
#include <unistd.h>
#include "util.h"
#include <vector>
#include <algorithm>
#include <unordered_set>

namespace std {


    tuple<unordered_map<uint32_t, graph_vector>, vector<uint32_t>> read_train_graph_vectors(string filename) {

      unordered_map<uint32_t, graph_vector> graph_vectors;
      vector<uint32_t> train_gids;
      ifstream f(filename);
      string line;

      while (getline(f, line)) {
        uint32_t gid;
        double e;
        stringstream ss;
        ss.str(line);
        ss >> gid;
        train_gids.push_back(gid);
        while (ss >> e) {
          graph_vectors[gid].push_back(e);
        }
      }
      return make_tuple(graph_vectors, train_gids);
    }

    vector<Branch> read_prototype_branches(string filename) {

      vector<Branch> prototype_branches;
      ifstream f(filename);
      string line;
      while (getline(f, line)) {
        Branch br;
        stringstream ss;
        ss.str(line);
        ss >> br.r;
        ss >> br.d_out;
        ss >> br.d_in;
        getline(f, line);
        if (br.d_out > 0) {
          stringstream ss;
          ss.str(line);
          string c;
          while (ss >> c) {
              int f;
            ss >> f;
            if (f > 0) br.es_out[c] = f;
          }
        }
        getline(f, line);
        if (br.d_in > 0) {
          stringstream ss;
          ss.str(line);
          string c;
          int f;
          while (ss >> c) {
            ss >> f;
            if (f > 0) br.es_in[c] = f;
          }
        }
        prototype_branches.push_back(br);
      }

      return prototype_branches;
    }

    tuple<uint32_t, unordered_map<uint32_t, vector<edge>>> read_edges
            (string filename, vector<uint32_t> &train_gids, unordered_set<uint32_t> scenarios) {


      cerr << "Reading edges from: " << filename << endl;
      vector<edge> train_edges;
      unordered_map<uint32_t, vector<edge>> test_edges;
      uint32_t num_train_edges = 0;
      ifstream f(filename);
      string line;


      // read edges from the file
      uint32_t i = 0;
      uint32_t max_gid = 0;


      while (getline(f, line)) {
        string src_type, dst_type, e_type;
        uint32_t src_id, dst_id, graph_id;
        stringstream ss;
        ss.str(line);
        ss >> src_id;
        ss >> src_type;
        ss >> dst_id;
        ss >> dst_type;
        ss >> e_type;
        ss >> graph_id;
        if (graph_id > max_gid) {
          max_gid = graph_id;
        }

        i++; // skip newline


        uint32_t scenario = graph_id / 100;
        if (scenarios.find(scenario) != scenarios.end()) {
          // add an edge to memory
          if (find(train_gids.begin(), train_gids.end(), graph_id) == train_gids.end()) { // is a test edge
            test_edges[graph_id].push_back(make_tuple(src_id, src_type,
                                                      dst_id, dst_type,
                                                      e_type, graph_id));

          }
        }
      }
      return make_tuple(max_gid + 1, test_edges);
    }

    tuple<vector<vector<uint32_t>>, vector<double>, double> read_bootstrap_clusters(string bootstrap_file) {
      int nclusters;
      double global_threshold;
      ifstream f(bootstrap_file);
      string line;
      stringstream ss;

      getline(f, line);
      int num_graphs;
      ss.str(line);
      ss >> nclusters >> num_graphs >> global_threshold;
      cout << nclusters << " , " << global_threshold << endl;
      vector<double> cluster_thresholds(nclusters);
      vector<vector<uint32_t>> clusters(nclusters);

      for (int i = 0; i < nclusters; i++) {
        getline(f, line);
        ss.clear();
        ss.str(line);

        double cluster_threshold;
        ss >> cluster_threshold;
        cluster_thresholds[i] = cluster_threshold;

        double gid;
        while (ss >> gid) {
          clusters[i].push_back((int) gid);
        }
      }

      return make_tuple(clusters, cluster_thresholds, global_threshold);
    }
}