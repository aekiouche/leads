

#ifdef DEBUG
#define NDEBUG            0
#endif

extern  int M;
#define B                 100
#define R                 20
#define BUF_SIZE          50
#define DELIMITER         '\t'
#define L                 1000     
#define SEED              23
#define CLUSTER_UPDATE_INTERVAL   100
#define INF		   5000000
#define UNSEEN          -2
#define ANOMALY -1


#define PI                3.1415926535897

#endif
