//
// Created by Abderrahmane on 9/10/2018.
//

#include "cluster.h"
#include <vector>
#include <cstdint>
#include <unordered_map>
#include <cmath>
#include <iostream>
#include "param.h"
#include "graph.h"

namespace std {
    vector <vector<double>> construct_centroid_vectors( unordered_map<uint32_t, vector<double>> &train_graph_vectors,
                               const vector <vector<uint32_t>> &clusters) {


        vector<vector<double>> centroid_vectors(clusters.size(),vector<double>(M,0.0));



        for (uint32_t c=0;c<clusters.size();c++){
            for (auto &gid : clusters[c]){
             // add the vector of this graph to the centroid's
                for (uint32_t m=0;m<M;m++){
                    centroid_vectors[c][m] += train_graph_vectors[gid].at(m);
                }
            }
        }
        cout << "end" << endl;
        // now the centroid vectors contain the sum of all projections of their cluster
        for (uint32_t c = 0; c < clusters.size(); c++) {
            for (uint32_t m=0;m<M;m++)  {
                centroid_vectors[c][m] /= clusters[c].size();
                cout << centroid_vectors[c][m] << ","<< endl;
            }
            cout << endl;
        }

        return centroid_vectors ;
    }
    void detect_anomalies(uint32_t &gid,const graph_vector &gv,
                          vector<int>& cluster_map,
                          vector<double>& anomaly_scores,
                          const vector<vector<double>> &centroid_vectors,
                          vector<uint32_t>& cluster_sizes,
                          const double &anomaly_threshold,
                          const vector<double>& cluster_thresholds){

        uint32_t nclusters = cluster_sizes.size();
        vector<double> distances(nclusters);
        double min_distance = INF;
        int nearest_cluster = -1;

        for (uint32_t i = 0; i < nclusters; i++) {
            distances[i] = euclidean_distance(gv, centroid_vectors[i]);
            if (distances[i] < min_distance) {
                min_distance = distances[i];
                nearest_cluster = i;
            }
        }
        // set its anomaly score to distance from nearest centroid
        anomaly_scores[gid] = min_distance;
        cluster_map[gid] = nearest_cluster;
        int current_cluster = cluster_map[gid];
        if (min_distance > min(anomaly_threshold,
                               cluster_thresholds[nearest_cluster])) {
            // change cluster mapping to ANOMALY
            cluster_map[gid] = ANOMALY;
        }
    }


}