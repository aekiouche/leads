
#ifndef STREAMSPOT_IO_H_
#define STREAMSPOT_IO_H_

#include "graph.h"
#include <string>
#include <tuple>
#include <vector>
#include <unordered_set>

namespace std {

    vector<Branch> read_prototype_branches(string filename);
    tuple<unordered_map<uint32_t, graph_vector>,vector<uint32_t>> read_train_graph_vectors(string filename);

    tuple<uint32_t,unordered_map<uint32_t,vector<edge>>> read_edges (string filename,vector<uint32_t> &train_gids,unordered_set<uint32_t> scenarios);


    tuple<vector<vector<uint32_t>>, vector<double>, double>  read_bootstrap_clusters(string bootstrap_file);
}

#endif
