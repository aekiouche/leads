//
// Created by Abderrahmane on 6/16/2018.
//
#include <algorithm>
#include <iostream>
#include <set>
#include <cmath>
#include "graph.h"
#include "hash.h"
#include "param.h"


namespace std {



    double compute_branch_edit_distance(Branch &Br1, Branch &Br2) {
        double bed = 0;
        double max_bed = 1+max(Br1.d_out, Br2.d_out)+max(Br1.d_in, Br2.d_in);
        if (Br1.r != Br2.r) bed += 1;
        bed += max(Br1.d_out, Br2.d_out);
        bed += max(Br1.d_in, Br2.d_in);
        if (max(Br1.d_out, Br2.d_out) == Br2.d_out) {
            for (auto &a : Br1.es_out)bed -= min(a.second, Br2.es_out[a.first]);
        } else {
            for (auto &a : Br2.es_out)bed -= min(a.second, Br1.es_out[a.first]);
        }
        if (max(Br1.d_in, Br2.d_in) == Br2.d_in) {
            for (auto &a : Br1.es_in)bed -= min(a.second, Br2.es_in[a.first]);
        } else {
            for (auto &a : Br2.es_in)bed -= min(a.second, Br1.es_in[a.first]);
        }
        return bed/max_bed;
    }

    double euclidean_distance(const vector<double> &a,const vector<double> &b){
        double dist = 0;
        for (int i=0;i<M;i++){
            dist += pow(abs(a[i]-b[i]),2);
        }
        return  sqrt(dist);
    }

    void update_BED(edge &e,uint32_t &branch_id,Branch &Br,unordered_map<uint32_t,vector<double>>& branches_distances,
                     vector<Branch> &prototype_branches,double &graph_size){

        // read the edge
        uint32_t  src_id = get<F_S>(e);
        uint32_t  dst_id = get<F_D>(e);
        string src_type = get<F_STYPE>(e);
        string dst_type = get<F_DTYPE>(e);
        string e_type = get<F_ETYPE>(e);
        if (src_id==branch_id) // is a source branch
        {
            for (int i=0;i<M;i++){
                double max_bed= 1+max(Br.d_out, prototype_branches.at(i).d_out)+
                                max(Br.d_in, prototype_branches.at(i).d_in);
                double bed = branches_distances[branch_id].at(i)*max_bed;
                bed = round(bed);
                if (((Br.d_out+1) > prototype_branches.at(i).d_out) &&
                ( ( Br.es_out[e_type] +1) > prototype_branches.at(i).es_out[e_type] )){
                    bed++;
                }
                else if (((Br.d_out+1) <= prototype_branches.at(i).d_out) &&
                         ( ( Br.es_out[e_type] +1)<= prototype_branches.at(i).es_out[e_type] )){
                    bed--;
                }

                double new_max = 1+max(Br.d_out+1, prototype_branches.at(i).d_out)+
                                 max(Br.d_in, prototype_branches.at(i).d_in);
                branches_distances[branch_id].at(i) =bed/new_max;
                if ((bed / new_max)<0 || (bed / new_max)>1) exit(35);
            }
            // update the branch
            Br.d_out++;
            Br.es_out[e_type]++;
        }
        if (dst_id ==branch_id) // is a dst branch
        {
            for (int i=0;i<M;i++){
                double max_bed= 1+max(Br.d_out, prototype_branches.at(i).d_out)+
                                max(Br.d_in, prototype_branches.at(i).d_in);
                double bed = branches_distances[branch_id].at(i)*max_bed;
                bed = round(bed);
                if (((Br.d_in+1) > prototype_branches.at(i).d_in) &&
                    ( ( Br.es_in[e_type] +1)> prototype_branches.at(i).es_in[e_type] )){
                    bed++;
                }
                else if (((Br.d_in+1) <= prototype_branches.at(i).d_in) &&
                         ( ( Br.es_in[e_type] +1)<= prototype_branches.at(i).es_in[e_type] )){
                    bed--;
                }
                double new_max = 1+max(Br.d_out, prototype_branches.at(i).d_out)+
                                 max(Br.d_in+1, prototype_branches.at(i).d_in);
                branches_distances[branch_id].at(i) =bed/new_max;
                if ((bed / new_max)<0 || (bed / new_max)>1) exit(35);
            }
            // update the branch
            Br.d_in++;
            Br.es_in[e_type]++;
        }
    }


    void update_graph_vector2 (uint32_t  &gid,edge   &e,
                               vector<Branch> &prototype_branches,
                               unordered_map<uint32_t, unordered_map<uint32_t, vector<double>>> &graphs_to_branches_distances,
                               unordered_map<uint32_t, unordered_map<uint32_t, Branch>> &graphs_to_branches,
                               unordered_map<uint32_t, graph_vector> &graphs_vectors,
                               unordered_map<uint32_t ,double > &graphs_sizes){

        // read edge
        uint32_t  src_id = get<F_S>(e);
        uint32_t  dst_id = get<F_D>(e);
        string src_type = get<F_STYPE>(e);
        string dst_type = get<F_DTYPE>(e);
        string e_type = get<F_ETYPE>(e);


        // update src_id branch
        graphs_to_branches[gid][src_id].r = src_type;
        uint32_t  d_out = graphs_to_branches[gid][src_id].d_out;
        uint32_t  d_in = graphs_to_branches[gid][src_id].d_in;
        if ( (d_out+d_in)==0 ) {   // a new branch
            graphs_sizes[gid]+=1;
            graphs_to_branches[gid][src_id].d_out=1;
            graphs_to_branches[gid][src_id].es_out[e_type]=1;
            int k = 0;

            for (auto &brp:prototype_branches) {
                double bed = compute_branch_edit_distance(graphs_to_branches[gid][src_id], brp);
                graphs_vectors[gid].at(k) *= 2*(graphs_sizes[gid]-1);
                graphs_vectors[gid].at(k) += (1-bed);
                graphs_vectors[gid].at(k)/=2*graphs_sizes[gid];
                graphs_to_branches_distances[gid][src_id].push_back(bed);
                k++;
            }
        }
        else{   // a old branch

            // substract the old impact of the branch src_id
            for (int k=0;k<M;k++) {
                graphs_vectors[gid].at(k) *= (2*graphs_sizes[gid]);
                graphs_vectors[gid].at(k) -=  (1-graphs_to_branches_distances[gid][src_id].at(k)) * (graphs_to_branches[gid][src_id].d_in
                                                                                                     + graphs_to_branches[gid][src_id].d_out);
            }
            // update the BED's
            graphs_sizes[gid]+=1;
            update_BED(e,src_id,graphs_to_branches[gid][src_id],graphs_to_branches_distances[gid],prototype_branches,graphs_sizes[gid]);

            // add the new impact of the branch src_id

            for (int k=0;k<M;k++) {
                graphs_vectors[gid].at(k) += (1- graphs_to_branches_distances[gid][src_id].at(k))*(graphs_to_branches[gid][src_id].d_in
                                                                                                   + graphs_to_branches[gid][src_id].d_out);
                graphs_vectors[gid].at(k) /= (2*graphs_sizes[gid]);
            }
        }
        // update dst_id branch
        graphs_to_branches[gid][dst_id].r = dst_type;
        d_out = graphs_to_branches[gid][dst_id].d_out;
        d_in = graphs_to_branches[gid][dst_id].d_in;
        if ( (d_out+d_in)==0 ) {   // a new branch
            graphs_to_branches[gid][dst_id].d_in=1;
            graphs_to_branches[gid][dst_id].es_in[e_type]=1;
            int k = 0;
            for (auto &brp:prototype_branches) {
                double bed = compute_branch_edit_distance(graphs_to_branches[gid][dst_id], brp);
                graphs_vectors[gid].at(k) *= 2*graphs_sizes[gid];
                graphs_vectors[gid].at(k) += (1-bed);
                graphs_vectors[gid].at(k)/=2*graphs_sizes[gid];
                graphs_to_branches_distances[gid][dst_id].push_back(bed);
                k++;
            }
        }
        else{   // a old branch
            // substract the old impact of the branch src_id
            for (int k=0;k<M;k++) {
                graphs_vectors[gid].at(k) *= (2*graphs_sizes[gid]);
                graphs_vectors[gid].at(k) -=  (1-graphs_to_branches_distances[gid][dst_id].at(k))*(graphs_to_branches[gid][dst_id].d_in
                                                                                                  + graphs_to_branches[gid][dst_id].d_out);
            }
            // update the BED's
            update_BED(e,dst_id,graphs_to_branches[gid][dst_id],graphs_to_branches_distances[gid],prototype_branches,graphs_sizes[gid]);

            // add the new impact of the branch src_id
            for (int k=0;k<M;k++) {
                graphs_vectors[gid].at(k) += (1- graphs_to_branches_distances[gid][dst_id].at(k))*(graphs_to_branches[gid][dst_id].d_in
                                                                                                   + graphs_to_branches[gid][dst_id].d_out);
                graphs_vectors[gid].at(k) /= (2*graphs_sizes[gid]);
            }
        }
    }




}
