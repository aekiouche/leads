

#include <algorithm>
#include <bitset>
#include <cassert>
#include <deque>
#include <iostream>

#include <string>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <sstream>
#include <random>
#include <chrono>
#include <fstream>


#include "docopt.h"
#include "graph.h"
#include "hash.h"
#include "io.h"
#include "param.h"
#include "cluster.h"


using namespace std;

static const char USAGE[] =
        R"( LEADS (Anomaly detection phase).

    Usage:
    LEADS     --edges=<edge file>
         --prototypes=<prototype branches file>
		 --train=<train graphs file>
         --clusters=<bootstrap cluster file>
         --dataset=<dataset>
         --B=<par>
         --out=<output>

   LEADS (-h | --help)

    Options:
      -h, --help                              Show this screen.
      --edges=<edge file>                     Incoming stream of edges.
      --prototypes=<prototype branches file>  The prototype branhces
      --train=<train graphs file>	          train graph vectors
      --clusters=<bootstrap cluster file>     bootstrap cluster file
      --dataset=<dataset>                     'ALL', 'YDC', 'GFC', 'YDG','AUTH,.
      --B=<par>                               number of parallel graphs that arrive simulatenously
)";
int M;
int main(int argc, char *argv[]) {

    // for measuring time
    chrono::time_point<chrono::steady_clock> start;
    chrono::time_point<chrono::steady_clock> end;
    chrono::nanoseconds diff;
    // for random
    mt19937_64 prng(SEED);


    // arguments
    map<string, docopt::value> args = docopt::docopt(USAGE, {argv + 1, argv + argc});
    string edge_file(args["--edges"].asString());
    string prototypes_file(args["--prototypes"].asString());
    string train_vector_file(args["--train"].asString());
    string cluters_file (args["--clusters"].asString());
    string dataset(args["--dataset"].asString());
    string output_file(args["--out"].asString());
    uint32_t num_graphs;

    vector<uint32_t> train_gids;
    unordered_map<uint32_t,vector<edge>> test_edges;
    long par(args["--B"].asLong());



    if (!(dataset.compare("ALL") == 0 ||
          dataset.compare("AUTH")== 0 ||
          dataset.compare("YDC") == 0 ||
            dataset.compare("YDG") == 0 ||
          dataset.compare("GFC") == 0)) {
        cout << "Invalid dataset: " << dataset << ". ";
        exit(-1);
    }
    unordered_set<uint32_t> scenarios;
    if (dataset.compare("GFC") == 0) {
        scenarios.insert(1);
        scenarios.insert(2);
        scenarios.insert(5);
        scenarios.insert(3); // attack
    } else if (dataset.compare("YDC") == 0) {
        scenarios.insert(0);
        scenarios.insert(4);
        scenarios.insert(5);
        scenarios.insert(3); // attack
    }else if (dataset.compare("YDG")==0) {
        scenarios.insert(0);
        scenarios.insert(4);
        scenarios.insert(1);
        scenarios.insert(3); // attack
        scenarios.insert(6);
    }else if (dataset.compare("AUTH")==0){
        scenarios.insert(0);
        scenarios.insert(1);
    } else { // ALL
        scenarios.insert(0);
        scenarios.insert(1);
        scenarios.insert(2);
        scenarios.insert(3); // attack
        scenarios.insert(4);
        scenarios.insert(5);
        scenarios.insert(6);
        scenarios.insert(7);
    }

    // the prototype branches
    vector<Branch> prototype_branches;
    prototype_branches = read_prototype_branches(prototypes_file);
    M = prototype_branches.size();
    cout << "M = " << M << endl;
    // read train graph vectors branches
    unordered_map<uint32_t, graph_vector> graphs_vectors; // key = gid , value = the graph vector
    tie (graphs_vectors,train_gids) = read_train_graph_vectors(train_vector_file);
    // read bootstrap clusters and thresholds
    vector<vector<uint32_t>> clusters;
    vector<double> cluster_thresholds;
    double global_threshold;
    tie(clusters, cluster_thresholds, global_threshold) = read_bootstrap_clusters(cluters_file);

    // read the test edges

    tie(num_graphs,test_edges) = read_edges(edge_file, train_gids,scenarios);
        ///num_graphs = 600;




    // initialization of graphs vectors
    for (int i=0;i<num_graphs;i++){
        if (find(train_gids.begin(),train_gids.end(),i)==train_gids.end()) { // is a test graph
            for (int j=0;j<M;j++) graphs_vectors[i].push_back(0);
        }
    }
    num_graphs = 700;
    if (dataset.compare("AUTH")==0){
        num_graphs = 143;
    }
    vector<int> cluster_map(num_graphs, UNSEEN); // gid -> cluster id

    unordered_map<uint32_t, unordered_map<uint32_t, Branch>> graphs_to_branches ;

    unordered_map<uint32_t ,double>  graphs_sizes; // key = gid ; value = the size of the graph
    // initialization of graphs size
    int gid = 0;
    for (int i=0;i<num_graphs;i++){
          graphs_sizes[gid]=0;
          gid++;
    }

    // make groups of size par (parallel flowing graphs)
    vector<uint32_t> test_gids;
    for (uint32_t i = 0; i < num_graphs; i++) {
        uint32_t scenario = i / 100;
        if (scenarios.find(scenario) != scenarios.end()) {
            if (find(train_gids.begin(), train_gids.end(), i) == train_gids.end()) {
                test_gids.push_back(i);
            }
        }
    }
    shuffle(test_gids.begin(), test_gids.end(), prng);
    vector<vector<uint32_t>> groups;
    for (uint32_t i = 0; i < test_gids.size(); i += par) {
        vector<uint32_t> group;

        uint32_t end_limit;
        if (test_gids.size() - i < par) {
            end_limit = test_gids.size() - i;
        } else {
            end_limit = par;
        }

        for (uint32_t j = 0; j < end_limit; j++) {
            group.push_back(test_gids[i + j]);
        }
        groups.push_back(group);
    }

    /// create train graph centers

    vector<vector<double>> centroid_vectors;
    // construct cluster centroid sketches/projections
    cout << "Constructing bootstrap cluster centroids:" << endl;
    centroid_vectors = construct_centroid_vectors(graphs_vectors,clusters);
    cout << "end" << endl;
    /// cluster map initialization
    cout << clusters.size() << endl;
    vector<uint32_t> cluster_sizes(clusters.size());
    for (uint32_t i = 0; i < clusters.size(); i++) {
        cluster_sizes[i] = clusters[i].size();
        for (auto& gid : clusters[i]) {
            cluster_map[gid] = i;
        }
    }
    /// compute distances of training graphs to their cluster centroids
    vector<double> anomaly_scores(num_graphs, UNSEEN);
    for (auto& gid : train_gids) {
        // anomaly score is a "distance"
        anomaly_scores[gid] = euclidean_distance(graphs_vectors[gid],centroid_vectors[cluster_map[gid]]);
    }
    cout << "end anomaly initialization" << endl;
    // add test edges to graphs
    uint32_t  num_test_edges = 0;
    for (auto &me : test_edges) num_test_edges+= me.second.size();

    cout << "Streaming in " << num_test_edges << " test edges:" << endl;
    uint32_t num_intervals = ceil(static_cast<double>(num_test_edges) /
                                  CLUSTER_UPDATE_INTERVAL);
    if (num_intervals == 0)
        num_intervals = 1; // if interval length is too long
    vector<vector<double>> anomaly_score_iterations(num_intervals,
                                                    vector<double>(num_graphs));
    vector<vector<int>> cluster_map_iterations(num_intervals,
                                               vector<int>(num_graphs));
    cout << "begin group loop" << endl;
    uint32_t edge_num = 0;
    vector<uint32_t> anoumalous_gids;
    if (dataset.compare("AUTH")==0){
        anoumalous_gids.push_back(139);
        anoumalous_gids.push_back(140);
        anoumalous_gids.push_back(141);
        anoumalous_gids.push_back(142);
    }
    else{
        for (uint32_t gid =300;gid<400;gid++) anoumalous_gids.push_back(gid);
        for (uint32_t gid =600;gid<700;gid++) anoumalous_gids.push_back(gid);
    }

    start = chrono::steady_clock::now();
    for (auto& group : groups) {
        /// added

        unordered_map<uint32_t, unordered_map<uint32_t, vector<double>>> graphs_to_branches_distances;

        unordered_map<uint32_t,uint32_t> edge_offset;
        for (auto &g : group)
            edge_offset[g] = 0;

        vector<uint32_t> group_copy(group);

        while (group_copy.size() > 0) {

            // used to pick a random graph
            uniform_int_distribution<uint32_t> rand_group_idx(0, group_copy.size() - 1);

            uint32_t gidx = rand_group_idx(prng);
            uint32_t gid = group_copy[gidx];
            uint32_t off = edge_offset[gid];
            auto& e = test_edges[gid][off];
            if (get<F_GID>(e)!=gid ) exit(70);
            // process edge

            auto old_gv = graphs_vectors[gid];

            update_graph_vector2 (gid,e,
                                        prototype_branches,
                                       graphs_to_branches_distances,
                                       graphs_to_branches,
                                       graphs_vectors,
                                       graphs_sizes);


            // store current anomaly scores and cluster assignments
            detect_anomalies(gid,graphs_vectors[gid],cluster_map,anomaly_scores,centroid_vectors,cluster_sizes,global_threshold,cluster_thresholds);
            if ( edge_num % CLUSTER_UPDATE_INTERVAL ==0 || edge_num == num_test_edges-1 ){
              //  cout << edge_num << endl;
                anomaly_score_iterations[edge_num/CLUSTER_UPDATE_INTERVAL] = anomaly_scores;
                cluster_map_iterations[edge_num/CLUSTER_UPDATE_INTERVAL] = cluster_map;
            }
            edge_num++;
            edge_offset[gid]++; // increment next edge offset
            if (edge_offset[gid] == test_edges[gid].size()) {
                // out of edges for this gid
                group_copy.erase(group_copy.begin() + gidx);
                graphs_to_branches[gid].clear();
                test_edges[gid].clear();
            }
        }
        int  TP = 0;
        int FP=0;
        int  FN=0;
        int  TN = 0;
        for (uint32_t j = 0; j < num_graphs ; j++) {
            int k = cluster_map_iterations[edge_num / CLUSTER_UPDATE_INTERVAL][j];
            if (  find(anoumalous_gids.begin(),anoumalous_gids.end(),j)!=anoumalous_gids.end()) {
              //  cout <<  cluster_map_iterations[edge_num / CLUSTER_UPDATE_INTERVAL][113] << endl;
                if ( k== ANOMALY && k!=UNSEEN) TP++;
                if ( k!=UNSEEN && k!=ANOMALY) FN++;
            }
            else {
                if ( k!=UNSEEN && k!=ANOMALY) TN++;
                if (k==ANOMALY) FP++;
            }
        }
        cout << "scores : " << TP <<" "<< FP << " " << TN << " "<< FN <<" " << endl;
        for (auto& item:graphs_to_branches_distances) {
            for (auto& item1:item.second) vector<double>().swap(item1.second);
        }
        graphs_to_branches_distances.clear();
    }
    end = chrono::steady_clock::now();
    diff = chrono::duration_cast<chrono::nanoseconds>(end - start);
    cout << static_cast<double>(diff.count()) << "us" << endl;

    ofstream out_file;
    out_file.open(output_file);

   // cout << "Iterations " << num_intervals << endl;
    for (uint32_t i = 0; i < num_intervals; i++) {
        auto& a = anomaly_score_iterations[i];
        auto& c = cluster_map_iterations[i];
        for (uint32_t j = 0; j < a.size(); j++) {
            out_file << a[j] << ",";
        }
        out_file << endl;
        for (uint32_t j = 0; j < a.size(); j++) {
            out_file << c[j] << ",";
        }
        out_file << endl;
    }
    out_file << "execution time : " << static_cast<double>(diff.count()) << "ns" << endl;
        cout << "execution time : " << static_cast<double>(diff.count()) << "ns" << endl;

    return 0;
}
