//
// Created by Abderrahmane on 9/10/2018.
//

#ifndef GRAPH_CLUSTER_H
#define GRAPH_CLUSTER_H


#include <cstdint>
#include <vector>
#include <unordered_map>
#include "graph.h"

namespace std {
    vector<vector<double>> construct_centroid_vectors(unordered_map<uint32_t, vector<double>> &train_graph_vectors,
                                                      const vector<vector<uint32_t>> &clusters);

    void detect_anomalies(uint32_t &gid,const graph_vector &gv,
                          vector<int>& cluster_map,
                          vector<double>& anomaly_scores,
                          const vector<vector<double>> &centroid_vectors,
                          vector<uint32_t>& cluster_sizes,
                          const double &anomaly_threshold,
                          const vector<double>& cluster_thresholds);

}

#endif //GRAPH_CLUSTER_H
