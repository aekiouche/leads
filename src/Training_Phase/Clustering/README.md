I don't own this part of code. This code is originally based on Streamspot's clustering algorithm with slight modificiations.

Original source :  https://github.com/sbustreamspot/sbustreamspot-train


For the clustering part we've used k-medoids algorithm proposed by (Emaad manzoor et al 2016) with slight modifications.


References : 

MANZOOR, Emaad, MILAJERDI, Sadegh M., et AKOGLU, Leman. Fast memory-efficient anomaly detection in streaming heterogeneous graphs. 
In : Proceedings of the 22nd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining. 2016. p. 1035-1044.